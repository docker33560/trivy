
[![linuxserver.io](https://stef33560.gitlab.io/img/avatar.png)](https://stef33560.gitlab.io/docs/docker/outillage#d%C3%A9tection-des-failles-de-s%C3%A9curit%C3%A9-cve "That's here I'm talking about it")


![](https://static.scarf.sh/a.png?x-pxid=d0f5bc2c-1112-4c2f-b005-5004794b8b47)
# [docker33560/trivy](https://gitlab.com/docker33560/trivy)

[![Scarf.io pulls](https://scarf.sh/installs-badge/stef/docker33560%2Ftrivy?package-type=docker)](https://scarf.sh/gateway/stef33560/docker/docker33560%2Ftrivy)
[![GitLab Stars](https://img.shields.io/gitlab/stars/docker33560/trivy.svg?style=for-the-badge&logo=gitlab)](https://gitlab.com/docker33560)
[![GitLab Release](https://img.shields.io/gitlab/v/release/docker33560/trivy?style=for-the-badge&logo=gitlab)](https://gitlab.com/docker33560/trivy/releases)
[![GitLab Container Registry](https://img.shields.io/static/v1.svg?style=for-the-badge&label=stef33560&message=GitLab%20Registry&logo=gitlab)](https://gitlab.com/docker33560/trivy/container_registry)

[aquasec/trivy](https://aquasecurity.github.io/trivy) est un analyseur permettant entre autres de détecter des CVE. Plus d'infos dispo sur [mon site](https://stef33560.gitlab.io/docs/docker/outillage#d%C3%A9tection-des-failles-de-s%C3%A9curit%C3%A9-cve) 

## Usage

Voici quelques exemples pour créer un conteneur.

### docker-compose (mode serveur uniquement)

```yaml
---
version: "2.1"
services:
  trivy:
    image: stef.docker.scarf.sh/docker33560/trivy
    command: server --listen 0.0.0.0:4954  
    container_name: trivy-server
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /tmp/.cache:/root/.cache
    restart: unless-stopped
```

### docker cli

En mode client :

```bash
docker run --rm \
  --network="trivy" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v ~/.cache:/root/.cache \
  stef.docker.scarf.sh/docker33560/trivy
```

En mode serveur :

```bash
docker run --rm \
  --name trivy-server
  --network="trivy" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v ~/.cache:/root/.cache \
  stef.docker.scarf.sh/docker33560/trivy \
  server --listen 0.0.0.0:4954  
```

### Scan offline

Instancier `stef.docker.scarf.sh/docker33560/trivy` avec `--skip-update --offline-scan trivy` est possible au premier lancement puisque le build contient une version [des bases](#versions).

_**Attention**_ : ce n'est pas une raison pour mettre à jour régulièrement les bases par script à coté de l'image. Lui spécifier `-v ~/.cache:/root/.cache` permettra d'écraser les bases embarquées au profit des nouvelles bases updatées !

### Environnements remappés

Trivy ayant besoin d'accéder à `/var/run/docker.sock`, si votre environnement est [remappé](https://stef33560.gitlab.io/docs/docker/securite#le-user-namespace-point-godwin-de-docker) pour que Docker ne tourne pas avec le user `root`, il vous faudra l'exécuter en tant que root ! 

A cet effet `docker run -u 0` fera tomber Trivy en marche ;)

```bash
docker run --rm -u 0\
  --network="trivy" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v ~/.cache:/root/.cache \
  stef.docker.scarf.sh/docker33560/trivy
```

## Build

### local

```bash
git clone https://gitlab.com/docker33560/trivy.git docker33560-trivy
cd stef33560-trivy
docker build \
  --no-cache \
  --pull \
  --build-arg TRIVY_VERSION=${TRIVY_VERSION} #optionnal
  -t docker pull stef.docker.scarf.sh/docker33560/trivy:latest .

```

`${TRIVY_VERSION}` correspond au [numéro de version](https://github.com/aquasecurity/trivy/releases) de Trivy **sans le `v`** (par défaut [0.34.0](https://gitlab.com/docker33560/trivy/-/blob/main/Dockerfile#L2)).

### Automatique

Adossée à une [Crontab](https://gitlab.com/docker33560/trivy/-/pipeline_schedules) exécutée tous les jours à 07.30 GMT+1, ce dépot se met à jour quotidiennement via sa [GitLab CI](./.gitlab-ci.yml) pour rappatrier la base de données de Trivy. Cela permet de s'en servir de [mirror](https://git-scm.com/docs/git-clone#Documentation/git-clone.txt---bare) à destination d'un environnement airgapped.

## Versions

* **11.11.2022:** - Initial Release
* **13.11.2022:** - 📦 NEW: database autosync
* **2022-11-13T18:56:22Z** - Update 60ec18e8
* **14.11.2022:** 📦 NEW: adds template


* **2022-11-14T20:40:11Z** - Update 0cfae0b0

* **2022-11-14T21:54:44Z** - Update 0cd7c668

* **2022-11-14T22:17:11Z** - Update cab14c96

* **2022-11-15T06:32:10Z** - Update d51398f9

* **2022-11-16T06:32:42Z** - Update dd40f5a3

* **2022-11-17T06:33:55Z** - Update c2059764

* **2022-11-18T06:33:24Z** - Update 06a1dfa2

* **2022-11-25T06:34:27Z** - Update 43b1fcc2

* **2022-12-02T06:35:43Z** - Update 5596af1d

* **2022-12-09T06:35:22Z** - Update 4fa55f76

* **2022-12-23T06:36:07Z** - Update ab429fc3

* **2022-12-30T06:36:33Z** - Update 8f608c4b

* **2023-01-06T06:37:29Z** - Update fdc9da74

* **2023-01-13T06:37:58Z** - Update 769a8262

* **2023-01-20T06:38:46Z** - Update 0a2467c9

* **2023-01-27T06:39:09Z** - Update 09c1a72c

* **2023-02-10T06:40:42Z** - Update 9db003a3

* **2023-02-24T06:41:31Z** - Update c2a655fd

* **2023-03-03T06:43:08Z** - Update ba5d4069

* **2023-03-10T06:43:26Z** - Update 5552192f

* **2023-03-17T06:44:12Z** - Update b016e876

* **2023-03-24T06:43:56Z** - Update 64bbac6a

* **2023-03-31T05:32:52Z** - Update 3d4cfc35

* **2023-04-07T05:34:02Z** - Update 81052360

* **2023-04-14T05:34:39Z** - Update 190e6aab

* **2023-04-21T05:35:21Z** - Update e81aceea

* **2023-04-28T05:36:09Z** - Update 73c7b1e2

* **2023-05-05T05:37:21Z** - Update 2dec10cc

* **2023-05-12T05:37:52Z** - Update 4316bc45

* **2023-05-19T05:37:33Z** - Update 52adc873

* **2023-05-26T05:38:42Z** - Update 90ae2ea6

* **2023-06-02T05:38:27Z** - Update 38da48c8

* **2023-06-09T05:32:29Z** - Update ac39c12f

* **2023-06-16T05:33:13Z** - Update cbab5238

* **2023-06-23T05:33:45Z** - Update 4d04406d

* **2023-06-30T05:34:22Z** - Update 26024640

* **2023-07-07T05:35:15Z** - Update e98a97fa

* **2023-07-14T05:35:36Z** - Update 280a49ee

* **2023-07-28T05:36:45Z** - Update 14c22ec9

* **2023-08-04T05:37:36Z** - Update c725c019

* **2023-08-18T05:35:07Z** - Update 30bf549e

* **2023-08-25T05:35:31Z** - Update 2f771a1d

* **2023-09-01T05:35:41Z** - Update 09267aea

* **2023-09-08T05:36:12Z** - Update 7a232839

* **2023-09-15T05:36:33Z** - Update 9d30fbed

* **2023-09-22T05:36:41Z** - Update 1c49bd33

* **2023-09-29T05:37:07Z** - Update 6877b5ea

* **2023-10-06T05:37:41Z** - Update d4220b1d

* **2023-10-13T05:37:55Z** - Update a2334b37

* **2023-10-20T05:38:16Z** - Update d119e7fa
