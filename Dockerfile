FROM alpine:3.16.2
ARG TRIVY_VERSION=0.34.0
ENV URL "https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz"
RUN apk --no-cache add ca-certificates tar gzip && \
    wget -O - $URL | tar -xzf - -C /tmp/ &&\
    mv /tmp/trivy /usr/local/bin
COPY trivy /root/.cache/trivy/
COPY contrib /contrib/
ENTRYPOINT ["trivy"]
